#####################################
GISNav Developer Documentation
#####################################
Welcome to GISNav's developer documentation!

This site contains the following pages:

    * `Get Started <pages/get_started.html>`_

        A quick demonstration of GNSS-free visual navigation with GISNav's :class:`.MockGPSNode`

    * `Setup <pages/setup.html>`_

        Instructions on how to to setup your own PX4 development and simulation environment

    * `Developer Guide <pages/developer_guide.html>`_

        Instructions on how to integrate GISNav with your own project and how to extend its functionality

    * `API Documentation <pages/api_documentation.html>`_

        GISNav public API reference for developers

    * `Contribute <pages/contribute.html>`_

        Contributions to GISNav are welcome! Please see this page for guidance.

.. seealso::

    GISNav is built on ROS 2 and is designed to work with PX4 Autopilot. You may also be interested in their
    documentation:

        * `ROS 2 Documentation <https://docs.ros.org/>`_
        * `PX4 Autopilot User Guide <https://docs.px4.io/master/en/>`_

.. toctree::
   :maxdepth: 2
   :hidden:

   pages/get_started
   pages/setup
   pages/developer_guide
   pages/api_documentation
   pages/contribute
