**************************************************
API Documentation
**************************************************
This is an automatically generated public API documentation for the `GISNav ROS 2 package`_.

Unless you want to contribute to GISNav itself or really get into the weeds of how GISNav works, you probably
will only be interested in the :class:`.BaseNode` and :class:`.PoseEstimator` class APIs.

.. _GISNav ROS 2 package: https://gitlab.com/px4-ros2-map-nav/python_px4_ros2_map_nav

Indices and Tables
=================================================
Use the search in the sidebar or these indices to find what you are looking for.

* :ref:`genindex`
* :ref:`modindex`


nodes package
=================================================
base_node module
--------------------------------------------
.. automodule:: gisnav.nodes.base_node
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

mock_gps_node module
--------------------------------------------
.. automodule:: gisnav.nodes.mock_gps_node
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

pose_estimators package
=================================================
pose_estimator module
--------------------------------------------
.. automodule:: gisnav.pose_estimators.pose_estimator
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

keypoint_pose_estimator module
--------------------------------------------
.. automodule:: gisnav.pose_estimators.keypoint_pose_estimator
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

loftr_pose_estimator module
--------------------------------------------
.. automodule:: gisnav.pose_estimators.loftr_pose_estimator
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

superglue_pose_estimator module
--------------------------------------------
.. automodule:: gisnav.pose_estimators.superglue_pose_estimator
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

data module
=================================================
.. automodule:: gisnav.data
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__, __post_init__
   :show-inheritance:

geo module
=================================================
.. automodule:: gisnav.geo
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

assertions module
=================================================
.. automodule:: gisnav.assertions
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

wms module
=================================================
.. automodule:: gisnav.wms
   :autosummary:
   :members:
   :undoc-members:
   :special-members: __init__
   :show-inheritance:

